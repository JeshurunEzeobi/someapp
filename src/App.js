import './App.css';
import React, { Fragment } from "react";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./styles/MaterialUI-Theme/theme";
import NoSsr from "@material-ui/core/NoSsr";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import SignIn from "./pages/SignIn/SignIn";
import SignUp from "./pages/SignUp/SignUp";
import Todo from "./pages/Todo/Todo";
import Error404 from "./pages/Error404/Error404";

function App() {
  
  return (
    <Fragment>
      <NoSsr>
        <MuiThemeProvider theme={theme}>
          <div>
            <Router>
              <div className="App">
              <Switch>
              <Route exact path="/" component={SignIn} />
              <Route exact path="/signup" component={SignUp} />
              <Route exact path="/todo" component={Todo} />
              <Route path={"*"} component={Error404} />
            </Switch>
              </div>
            </Router>
          </div>
        </MuiThemeProvider>
      </NoSsr>
    </Fragment>
  );
}

export default App;
