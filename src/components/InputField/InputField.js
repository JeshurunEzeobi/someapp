import React, {Fragment} from "react";
import { useField } from "formik";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  danger: {
    color: "#F76362",
    marginBottom: "4px",
  }
}));

const InputField = ({ ...props }) => {
  const classes = useStyles();
  const [field, meta] = useField(props);
  return(
    <Fragment>
    <TextField
      {...field}
      {...props}
      error={meta.touched && meta.error ? true : false}
      fullWidth
    />
    {meta.touched && meta.error ? (
      <div className={classes.danger}>{meta.error}</div>
    ) : null}
  </Fragment>
  );
};

export default InputField;
