import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useHistory } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    fontSize: "24px"
  },
  appbar: {
    backgroundColor: "#444D63"
  },
  user: {
    textTransform: "none"
  }
}));

const Navbar=({ onLogout })=> {
  
  const history = useHistory();
  const classes = useStyles();
  const [userEmail, setUserEmail] = useState("");
  
  useEffect(()=>{

    const token = localStorage.getItem("rodulfToken");
    const user = localStorage.getItem("currentUser");
    if (
      token === null ||
      token === "null" ||
      token === undefined 
    ) {
       history.push("/");
    }
    setUserEmail(user);
  },[]);

  return (
    <div className={classes.root}>
      <AppBar position="static" classes={{root: classes.appbar}}>
        <Toolbar classes={{root: classes.appbar}}>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <IconButton className={classes.menuButton} color="inherit" aria-label="menu">
          {userEmail}
           </IconButton>
          <Typography variant="h6" className={classes.title}>
            The Rudolf
          </Typography>
          <Button color="inherit" onClick={()=>{
              onLogout();
          }}>Logout</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default Navbar;