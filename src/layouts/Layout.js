import React from 'react';
import Navbar from "../components/Navbar/Navbar";
import { makeStyles } from "@material-ui/core/styles";
import { logoutUser } from "../store/slices/actions";
import { useDispatch } from "react-redux";


const useStyles=makeStyles(()=>({
   root: {
       width: "100%",
       height: "100vh",
   }
}));

const Layout=({children, ...props})=> {
    const dispatch = useDispatch();
    const classes = useStyles();
    const handleLogout=()=>{
        dispatch(logoutUser());
      }
    
    return (
        <div className={classes.root}>
        <Navbar onLogout={handleLogout}/>
            {children}
        </div>
    )
}

export default Layout
