import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import InputField from "../../components/InputField/InputField";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Typography from "@material-ui/core/Typography";
import homeIcon from "../../assets/images/home.svg";
import logo from "../../assets/images/logo.png";
import { Link } from "react-router-dom";
import Hidden from "@material-ui/core/Hidden";
import ReactLoading from "react-loading";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  input: {
    width: "90%",
    maxWidth: "450px",
    height: "66px",
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
  },
  button: {
    marginTop: "114px",
    width: "90%",
    maxWidth: "448px",
    height: "56px",
    background: "#444D63 0% 0% no-repeat padding-box",
    boxShadow: "3px 6px 9px #00000029",
    opacity: 1,
    font: "normal normal medium 24px/29px Montserrat",
    letterSpacing: "0px",
    color: "#FFFFFF",
    marginBottom: "16px",
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
  },
  icon: {
    backgroundImage: `url(${homeIcon})`,
    backgroundSize: "cover",
    minHeight: "100vh",
  },
  signinText: {
    fontFamily: " Montserrat",
    fontSize: "24px",
    letterSpacing: "0px",
    color: "#444D63",
    opacity: 1,
    marginBottom: "34px",
    width: "99px",
    height: "27px",
  },
  logo: {
    width: "90%",
    maxWidth: "403px",
    height: "60px",
    background: `transparent url(${logo}) 0% 0% no-repeat padding-box`,
    opacity: 1,
    marginTop: "20px",
    marginBottom: "120px",
    backgroundSize: "contain",
    backgroundPosition: "center",
    [theme.breakpoints.down("sm")]: {
      width: "80%",
      height: "30",
      marginBottom: "60px",
    },
  },
  link: {
    fontWeight: "bold",
    textDecoration: "none",
    color: "blue",
  },
  form: {
    minWidth: "80%",
  },
  loaderContainer: {
    display: "flex",
    justifyContent: "center",
    marginTop: "60px",
    marginBottom: "16px",
  }
}));

const SignInLayout = ({ onComplete, status }) => {
  const classes = useStyles();
  const [values, setValues] = useState({
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <Grid container>
    <Hidden smDown> <Grid item xs={12} sm={12} md={7} classes={{ root: classes.icon }}></Grid></Hidden>
      <Grid item xs={12} sm={12} md={5}>
        <div className={classes.root}>
          <div className={classes.logo}></div>
          <Typography classes={{ root: classes.signinText }}>
            Sign-in!
          </Typography>
          <Formik
            initialValues={{ email: "", password: "" }}
            validationSchema={Yup.object({
              password: Yup.string()
                .required("This field is required")
                .max(200, "Password should not exceed 200 characters"),
              email: Yup.string()
                .email("Invalid email address")
                .required("This field is required"),
            })}
            onSubmit={(values, { setSubmitting }) => {
              onComplete(values);
            }}
          >
            {() => (
              <Form className={classes.form}>
                <InputField
                  label={"EMAIL"}
                  name="email"
                  type="email"
                  margin="normal"
                  classes={{ root: classes.input }}
                  variant="outlined"
                />
                <InputField
                  label={"PASSWORD"}
                  name="password"
                  type={values.showPassword ? "text" : "password"}
                  margin="normal"
                  classes={{ root: classes.input }}
                  variant="outlined"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment>
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={(event) => {
                            handleMouseDownPassword(event);
                          }}
                        >
                          {values.showPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                {status === "loading"? <div className={classes.loaderContainer}>
                <ReactLoading type="bars" color="#444D63"  height={50} width={70}/>
                </div>:<Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  classes={{ root: classes.button }}
                >
                  Next
                </Button>}
              </Form>
            )}
          </Formik>
          <Typography color="textSecondary">
            Don't have an account{" "}
            <Link className={classes.link} to={"/signup"}>
              SignUp
            </Link>
          </Typography>
        </div>
      </Grid>
    </Grid>
  );
};

export default SignInLayout;
