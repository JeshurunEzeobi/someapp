import React, { useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Layout from "../Layout";
import Card from "@material-ui/core/Card";
import List from "../TodoLayout/components/List/List";
import { useSelector, useDispatch } from "react-redux";
import {
  getAllCategoryItemsAsync,
  getAllTodoItemsAsync,
  deleteCategoryItemAsync,
  addCategoryItemAsync,
  modifyCategoryItemAsync,
  deleteTodoItemAsync,
  modifyTodoItemAsync,
  addTodoItemAsync,
} from "../../store/slices/actions";
import { newCategoryId } from "../../store/slices/todo/listUtilSlice";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    position: "relative",
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "24px",
    fontWeight: 700,
    width: "100%",
    background: "#f5fcff",
    height: "50px",
  },
}));

const TodoLayout = () => {
  const dispatch = useDispatch();
  const activeCategoryId = useSelector((state) => state.listUtil.categoryId);
  const { categoryStatus, categoryItems } = useSelector(
    (state) => state.category
  );
  const { status, todoItems } = useSelector((state) => state.todo);

  const classes = useStyles();
  useEffect(() => {
    const getCategoryId = localStorage.getItem("categoryId");
    if (getCategoryId !== null && activeCategoryId === null) {
      dispatch(newCategoryId(getCategoryId));
      dispatch(getAllTodoItemsAsync({ categoryId: activeCategoryId }));
    }
  }, [activeCategoryId, dispatch]);

  useEffect(() => {
    dispatch(getAllCategoryItemsAsync());
  }, [dispatch]);

  useEffect(() => {}, []);

  const categories = [
    {
      _id: "6078dc349fc7390523i45f8c6one",
      userId: "6078d1a072e3d9032896343a",
      name: "education",
      createdOn: "2021-04-16T00:37:08.741Z",
    },
    {
      _id: "6078dc349fc73i9052344f8c6two",
      userId: "6078d1a072e3d9032896343a",
      name: "music",
      createdOn: "2021-04-16T00:42:18.741Z",
    },
    {
      _id: "6078dc349fc739752p345f8c6three",
      userId: "6078d1a072e3d9032896343a",
      name: "education",
      createdOn: "2021-04-16T00:37:08.741Z",
    },
    {
      _id: "60u78dc849fc7390523l44f8c6four",
      userId: "6078d1a072e3d9032896343a",
      name: "music",
      createdOn: "2021-04-16T00:42:18.741Z",
    },
    {
      _id: "6078dc3j49fc73k9752345f8c6five",
      userId: "6078d1a072e3d9032896343a",
      name: "education",
      createdOn: "2021-04-16T00:37:08.741Z",
    },
    {
      _id: "6178dc849fc739k052344f8c6six",
      userId: "6078d1a072e3d9032896343a",
      name: "music",
      createdOn: "2021-04-16T00:42:18.741Z",
    },
  ];

  const todosOfACategory = [
    {
      done: false,
      _id: "60794fb60d70520fe434c211",
      name: "cook food",
      categoryId: "60794051aea3f400c0354ecd",
      createdOn: "2021-04-16T08:49:58.892Z",
    },
    {
      done: false,
      _id: "60794ff40d70520fe434c212",
      name: "clean the room",
      categoryId: "60794051aea3f400c0354ecd",
      createdOn: "2021-04-16T08:51:00.215Z",
    },
  ];

  return (
    <Layout>
      <Grid spacing={0} container>
        <Grid classes={{ root: classes.root }} xs={12} sm={12} md={6} item>
          <Card classes={{ root: classes.title }}>Categories</Card>
          <List
            options={categoryItems}
            listType="category"
            currentCategoryId={activeCategoryId}
            label={"category"}
            onDelete={deleteCategoryItemAsync}
            onSave={modifyCategoryItemAsync}
            onCreate={addCategoryItemAsync}
            status={categoryStatus}
          />
        </Grid>
        <Grid classes={{ root: classes.root }} xs={12} sm={12} md={6} item>
          <Card classes={{ root: classes.title }}>Todos</Card>
          <List
            options={todoItems}
            listType="todo"
            currentCategoryId={activeCategoryId}
            label={"todo"}
            status={status}
            onDelete={deleteTodoItemAsync}
            onSave={modifyTodoItemAsync}
            onCreate={addTodoItemAsync}
          />
        </Grid>
      </Grid>
    </Layout>
  );
};

export default TodoLayout;
