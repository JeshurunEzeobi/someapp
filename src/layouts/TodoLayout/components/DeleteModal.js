import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";

const useStyles=makeStyles(()=>({
    title: {
        textTransform: "capitalize",
        fontSize: "24px",
        fontWeight: 700,
    },
    content: {
        color: "brown",
        fontSize: "14px",
    }
}))

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const DeleteModal=({ open, onClose, label, type, id, content, onDelete })=> {
const dispatch = useDispatch();
const classes = useStyles();
  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={onClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title"><span className={classes.title}>{label}</span></DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Do you wish to delete this entry permanently?
          </DialogContentText>
          <DialogContentText>
          <span className={classes.content}>{content}</span>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} variant="contained" color="primary">
            cancel
          </Button>
          <Button onClick={()=>{
              dispatch(onDelete({categoryId:id, todoId:id }));
              console.log(id);
              onClose()
            }} variant="contained" color="secondary">
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default DeleteModal;