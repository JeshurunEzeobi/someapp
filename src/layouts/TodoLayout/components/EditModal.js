import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';

const useStyles= makeStyles(()=>({
    title: {
        textTransform: "capitalize",
        fontSize: "24px",
        fontWeight: 700,
    }
}));

const EditModal=({ open, onClose, create, onSave, onCreate, label, type, id, categoryId,content, userId})=> {
  const classes = useStyles();  
  const dispatch = useDispatch();  
  const [inputText, setInputText] = useState(content);

  useEffect(()=>{
   setInputText(content);
  },[content]);

  const handleSave=()=>{
    if(!create){
    
      dispatch(onSave({todoId:id, categoryId:id, data: {name: inputText} }))
    }
  }

  const handleCreate=()=>{
    if(create){
    
      dispatch(onCreate({data:{name: inputText, categoryId}}))
    }
  }

  return (
    <div>
      <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
        <DialogTitle  ><span className={classes.title}>{label}</span></DialogTitle>
        <DialogContent>
          {create?<DialogContentText>
           Enter text, click on Create when you are done.
          </DialogContentText> : <DialogContentText>
           Enter text, click on save when you are done.
          </DialogContentText>}
          <TextField
            defaultValue={content}
            autoFocus
            name={label}
            margin="dense"
            label={label}
            type={type}
            fullWidth
            onChange={(event)=>{
               setInputText(event.target.value);
            }}
            multiline={true}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>{
              onClose();
            }} variant="contained" color="secondary">
            Cancel
          </Button>
          <Button onClick={()=>{
              onClose();
              console.log({name: inputText, userId, id, categoryId});
              handleCreate();
              handleSave();
             //dispatch(onSave({name: inputText, userId, id, categoryId}));
            }} variant="contained" color="primary">
            {create? "Create": "save"}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default EditModal;