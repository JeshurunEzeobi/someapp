import React, { Fragment, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import dayjs from "dayjs";
import clsx from "clsx";
import EditModal from "../EditModal";
import DeleteModal from "../DeleteModal";
import AddIcon from "@material-ui/icons/Add";
import Skeleton from "react-loading-skeleton";
import { useDispatch } from "react-redux";
import { newCategoryId } from "../../../../store/slices/todo/listUtilSlice";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    position: "relative",
    maxWidth: 600,
    maxHeight: 520,
    backgroundColor: theme.palette.background.paper,
    boxShadow: "3px 6px 9px #00000029",
    minHeight: 500,
    marginTop: 10,
    marginBottom: 10,
    [theme.breakpoints.down("sm")]: {
      minHeight: 200,
    },
  },
  list: {
    maxWidth: 600,
    maxHeight: 500,
    minHeight: 500,
    overflow: "auto",
    padding: 12,
    boxSizing: "border-box",
    [theme.breakpoints.down("sm")]: {
      minHeight: 180,
    },
  },
  listItem: {
    boxShadow: "3px 6px 9px #00000029",
    marginBottom: "4px",
    borderRadius: "5px",
    paddingRight: "56px",
  },
  actionContainer: {
    display: "flex",
    flexDirection: "row",
  },
  icons: {
    fontSize: "1.4rem",
    color: "#888888",
    "&:hover": {
      color: "rgb(7, 177, 77, 0.42)",
    },
  },
  listItemIcon: {
    minWidth: "30px",
  },
  deleteContainer: {
    marginLeft: "4px",
  },
  active: {
    background: "#afeeee",
    backgroundColor: "#afeeee",
  },
  secondary: {
    fontSize: 24,
    fontWeight: 700,
  },
  add: {
    borderRadius: "50%",
    padding: "4px",
    background: "#444D63",
    backgroundColor: "#444D63",
    position: "absolute",
    bottom: "10px",
    right: "10px",
    zIndex: 4,
  },
  addIcon: {
    fontSize: "35px",
    color: "white",
    "&:hover": {
      color: "#444D63",
    },
  },
  empty: {
    display: "flex",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
}));

const ListLayout = ({
  listType,
  onDelete,
  onSave,
  label,
  options,
  status,
  onCreate,
  currentCategoryId,
}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [checked, setChecked] = useState([]);
  const [open, setOpen] = useState(false);
  const [itemId, setItemId] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [createEntry, setCreateEntry] = useState(false);
  const [content, setContent] = useState("");
  const [active, setActive] = useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleCreate = () => {
    setCreateEntry(true);
  };

  const closeCreate = () => {
    setCreateEntry(false);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const hadleOpenDelete = () => {
    setOpenDeleteModal(true);
  };

  const closeDeleteModal = () => {
    setOpenDeleteModal(false);
  };
  const handleToggle = (value) => () => {
    setActive(value._id);
    const currentIndex = checked.indexOf(value._id);
    const newChecked = [...checked];

    if (listType === "category") {
      localStorage.setItem("categoryId", value._id);
      dispatch(newCategoryId(value._id));
    }

    if (currentIndex === -1) {
      newChecked.push(value._id);
      if(listType=== "todo"){
        dispatch(onSave({data:{done: true}, todoId: value._id}));
      }

    } else {
      newChecked.splice(currentIndex, 1);
      if(listType=== "todo"){
        dispatch(onSave({data:{done: true}, todoId: value._id}));
      }
    }

    setChecked(newChecked);
  };

  if (status === "loading") {
    return (
      <List className={classes.root}>
        <div>
          {listType === "category" ? (
            <Skeleton height={70} width={"100%"} count={5} />
          ) : (
            <Skeleton height={50} width={"100%"} count={5} />
          )}
        </div>
      </List>
    );
  }

  if (
    status === "idle" &&
    currentCategoryId === null &&
    listType === "todo"
  ) {
    return (
      <List className={classes.root}>
        <div className={classes.empty}>please select a Category...</div>
      </List>
    );
  }

  if (status === "error") {
    return (
      <List className={classes.root}>
        <div className={classes.empty}>Oops something went wrong...</div>
      </List>
    );
  }

  if (status === "idle" && options.length === 0) {
    return (
      <div className={classes.root}>
        <EditModal
          open={createEntry}
          onClose={closeCreate}
          onSave={onCreate}
          id={itemId}
          label={label}
          content={""}
          type={listType}
          create={true}
          onCreate={onCreate}
          categoryId={categoryId}
        />
        <IconButton
          color="primary"
          variant="contained"
          classes={{ root: classes.add }}
          onClick={(event) => {
            event.stopPropagation();
            setCategoryId(currentCategoryId);
            setContent("");
            handleCreate();
          }}
        >
          <AddIcon classes={{ root: classes.addIcon }} />
        </IconButton>
        <div className={classes.empty}>{`please create a ${listType}...`}</div>
      </div>
    );
  }
  return (
    <Fragment>
      <EditModal
        open={createEntry}
        onClose={closeCreate}
        onSave={onCreate}
        id={itemId}
        label={label}
        content={""}
        type={listType}
        create={true}
        onCreate={onCreate}
        categoryId={categoryId}
      />
      <DeleteModal
        open={openDeleteModal}
        onClose={closeDeleteModal}
        id={itemId}
        label={label}
        content={content}
        onDelete={onDelete}
      />
      <EditModal
        open={open}
        onClose={handleClose}
        onSave={onSave}
        id={itemId}
        label={label}
        content={content}
        type={listType}
        categoryId={categoryId}
      />
      <div className={classes.root}>
        <IconButton
          color="primary"
          variant="contained"
          classes={{ root: classes.add }}
          onClick={(event) => {
            event.stopPropagation();
            setCategoryId(currentCategoryId);
            setContent("");
            handleCreate();
          }}
        >
          <AddIcon classes={{ root: classes.addIcon }} />
        </IconButton>
        <List className={classes.list}>
          {options.map((value) => {
            const labelId = `checkbox-list-label-${value._id}`;

            return (
              <ListItem
                classes={{
                  root: clsx(
                    classes.listItem,
                    listType === "category" &&
                      active === value._id &&
                      classes.active
                  ),
                }}
                key={value._id}
                role={undefined}
                dense
                button
                onClick={handleToggle(value)}
              >
                {listType === "todo" ? (
                  <ListItemIcon classes={{ root: classes.listItemIcon }}>
                    <Checkbox
                      edge="start"
                      checked={checked.indexOf(value._id) !== -1}
                      tabIndex={-1}
                      disableRipple
                      inputProps={{ "aria-labelledby": labelId }}
                    />
                  </ListItemIcon>
                ) : null}
                <ListItemText
                  id={labelId}
                  primary={dayjs(value.createdOn).format("DD MMM YYYY")}
                  secondary={
                    listType === "category" ? (
                      <span className={classes.secondary}>{value.name}</span>
                    ) : (
                      value.name
                    )
                  }
                />
                <ListItemSecondaryAction
                  onClick={(event) => {
                    event.stopPropagation();
                  }}
                >
                  <div className={classes.actionContainer}>
                    <span
                      onClick={(event) => {
                        event.stopPropagation();
                        handleClickOpen();
                        setItemId(value._id);
                        setCategoryId(value.categoryId);
                        setContent(value.name);
                      }}
                    >
                      <EditIcon classes={{ root: classes.icons }} />
                    </span>
                    <span
                      classes={{ root: classes.deleteContainer }}
                      onClick={(event) => {
                        event.stopPropagation();
                        hadleOpenDelete();
                        setItemId(value._id);
                        setCategoryId(value.categoryId);
                        setContent(value.name);
                      }}
                    >
                      <DeleteIcon classes={{ root: classes.icons }} />
                    </span>
                  </div>
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
      </div>
    </Fragment>
  );
};

export default ListLayout;
