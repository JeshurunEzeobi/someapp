import React from 'react';
import Layout from '../../layouts/Layout';
import { makeStyles } from "@material-ui/core/styles";
import ErrorImage from "../../assets/images/banner_error_404.jpg";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme)=>({
  root: {
    background: `transparent url(${ErrorImage}) 0% 0% no-repeat padding-box`,
    backgroundSize: "contain",
    backgroundPosition: "center",
    width: "100%",
    height: "95vh",
    position: "relative",
  },
  button: {
      position: "absolute",
      top: 30,
      left: 30,
  }
}));

const Error404=()=> {
const classes = useStyles();
    return (
        <Layout>
         <div className={classes.root}>
         <Link to="/todo"><Button classes={{root: classes.button}} color="primary" variant="contained">Home</Button></Link>
         </div>
        </Layout>
    )
}

export default Error404;
