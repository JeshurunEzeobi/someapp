import React, { useEffect } from "react";
import SignInLayout from "../../layouts/SignInLayout/SignInLayout";
import { useDispatch, useSelector } from "react-redux";
import { loginUserAsync } from "../../store/slices/actions";
import { useHistory } from "react-router-dom";

const SignIn = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { token, status, user } = useSelector((state) => state.user);
  const handleLogin = (values) => {
    dispatch(loginUserAsync(values));
  };

  useEffect(() => {
    if (token !== "" && user !== null) {
      localStorage.setItem("rodulfToken", token);
      localStorage.setItem("currentUser", user.email);
    }
    const getToken = localStorage.getItem("rodulfToken");
    if (
      getToken !== null &&
      getToken !== "null" &&
      getToken !== undefined &&
      status === "idle"
    ) {
      history.push("/todo");
    }
  }, [token, status, history, user]);

  return (
    <div>
      <SignInLayout onComplete={handleLogin} status={status} />
    </div>
  );
};

export default SignIn;
