import React, { useEffect } from 'react';
import SignUpLayout from "../../layouts/SignUpLayout/SignUpLayout";
import { registerUserAsync } from "../../store/slices/actions";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";


const SignUp=()=> {
    const history = useHistory();
    const dispatch = useDispatch();
    const { token, status, user } = useSelector((state) => state.user);
    const handleRegister = (values) => {
      dispatch(registerUserAsync(values));
    };

    useEffect(() => {
        if (token !== "" && user !== null) {
          localStorage.setItem("rodulfToken", token);
          localStorage.setItem("currentUser",user.email);
        }
        const getToken = localStorage.getItem("rodulfToken");
        if (
          getToken !== null &&
          getToken !== "null" &&
          getToken !== undefined &&
          status === "idle"
        ) {
          history.push("/todo");
        }
      }, [token, status, history,user]);


    return (
        <div>
           <SignUpLayout onComplete={handleRegister} status={status}/> 
        </div>
    )
}

export default SignUp;
