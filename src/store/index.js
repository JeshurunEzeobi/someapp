import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import userReducer from "./slices/user/userSlice";
import categoryReducer from "./slices/category/categorySlice";
import todoReducer from "./slices/todo/todoSlice";
import listUtilReducer from './slices/todo/listUtilSlice';



const reducer = combineReducers({
	user: userReducer,
	category: categoryReducer,
	todo: todoReducer,
    listUtil: listUtilReducer,
})

export const store = configureStore({
  reducer,
});