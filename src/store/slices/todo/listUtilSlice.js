import { createSlice } from '@reduxjs/toolkit';


export const listUtilSlice = createSlice({
  name: 'listUtil',
  initialState: {
    categoryId: null,
  },
  reducers: {
    newCategoryId: (state, action) => {
      state.categoryId = action.payload;
    },
  },
});

export const { newCategoryId } = listUtilSlice.actions;

export default listUtilSlice.reducer;
