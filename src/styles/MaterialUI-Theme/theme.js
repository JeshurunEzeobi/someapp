import  createMuiTheme  from "@material-ui/core/styles/createMuiTheme";

const systemFont = 'Roboto, -apple-system, BlinkMacSystemFont, "Segoe UI", "Helvetica Neue", Ariel, sans-serif, "Apple Color Emoji", "Segoe UI Symbol"';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: "#065fe5",
            main:   "#2b4e7b",
            dark: "#4b4a4a",
            contrastText: "#fff",
        },
        secondary: {
           light: '#9FBEFE',
           main: '#F25E69',
           dark: '#DF5660',
           contrastText: '#FFF'
        },
        error: {
            light: '#F79EA5',
            main: '#F25E69',
            dark: '#DF5660',
            contrastText: '#FFF'
        },
        warning: {
           main: '#ffa826',
           light: '#FFF9F0',
           dark: '#CCA05E',
        },
        grey: {
            200: "#eeeeee",
            300: "#cacccd",
        },
        text: {
            primary: "#445268",
            secondary: "#687792",
            disabled: "#cacccd",
        }
     
     },

    typography: {
       fontFamily: systemFont, 
       fontSize: 14,
       fontWeightLight: 300,
       fontWeightRegular: 400,
       fontWeightMedium: 500,
       fontWeightBold: 700,
       h1: {
        fontFamily: systemFont,
        fontWeight: 700,
        fontSize: 32,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    h2: {
        fontFamily: systemFont,
        fontWeight: 700,
        fontSize: 24,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    h3: {
        fontFamily: systemFont,
        fontWeight: 700,
        fontSize: 16,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    h4: {
        fontFamily: systemFont,
        fontWeight: 700,
        fontSize: 14,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    h5: {
        fontFamily: systemFont,
        fontWeight: 300,
        fontSize: 14,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    h6: {
        fontFamily: systemFont,
        fontWeight: 300,
        fontSize: 12,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },

    subtitle1: {
        fontFamily: systemFont,
        fontWeight: 300,
        fontSize: 14,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    subtitle2: {
        fontFamily: systemFont,
        fontWeight: 300,
        fontSize: 12,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    body1: {
        fontFamily: systemFont,
        fontWeight: 400,
        fontSize: 14,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    body2: {
        fontFamily: systemFont,
        fontWeight: 300,
        fontSize: 12,
        lineHeight: 1.5,
        letterSpacing: "0em",
    },
    },

    
    
    
    
    

   
    //brand colors, you can extend the colors inline with demand
    
    
    
});

export default theme;

